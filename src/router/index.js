import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Scg from "../views/Scg.vue";
import Xyz from "../views/Xyz.vue";
import Place from "../views/Place.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/cv",
    name: "cv",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/CV.vue")
  },
  {
    path: "/scg",
    name: "scg",
    component: Scg
  },
  {
    path: "/xyz",
    name: "xyz",
    component: Xyz
  },
  {
    path: "/place",
    name: "place",
    component: Place
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
